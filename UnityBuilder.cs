using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.JSInterop;

public class UnityBuilder
{
    private readonly IJSRuntime js;

    public UnityBuilder(IJSRuntime js)
    {
        this.js = js;
    }

    public async ValueTask TickerChanged()
    {
        await js.InvokeVoidAsync("displayTickerAlert1", "document.querySelector(\"#unity-canvas\")", "{ dataUrl: \"Build/Build.data.br\", frameworkUrl: \"Build/Build.framework.js.br\", odeUrl: \"Build/Build.wasm.br\", streamingAssetsUrl: \"StreamingAssets\", companyName: \"DefaultCompany\", productName: \"ProjectInitiativeUnity\", productVersion: \"1.0\"}");
    }

    public void Dispose()
    {
    }
}