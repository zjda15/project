﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;


namespace Project_Initiative
{
    public class JsonFiles
    {
        public static async Task SaveCharacter(Character player)
        {
            string fileName = @"..\project\Json\"+player.name+".json";
            using FileStream stream = File.Create(fileName);
            await JsonSerializer.SerializeAsync(stream, player);
            await stream.DisposeAsync();
        }
        
        public static Character LoadCharacter(string path) 
        {
            var player = JsonSerializer.Deserialize<Character>(File.ReadAllText(@""+path)); 
            return player;
        }
    }
}
