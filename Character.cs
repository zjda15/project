﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Initiative
{
    public class Character
    {
        public string name { get; set; }
        public Races race { get; set; }
        public Classes classes { get; set; }
        public int speed { get; set; }
        public int strength { get; set; }
        public int dexterity { get; set; }
        public int constitution { get; set; }
        public int intelligence { get; set; }
        public int wisdom { get; set; }
        public int charisma { get; set; }

        //public int savingThrowStr  { get; set; }
        //public int savingThrowDex  { get; set; }
        //public int savingThrowCon  { get; set; }
        //public int savingThrowInt  { get; set; }
        //public int savingThrowWis  { get; set; }
        //public int savingThrowCha  { get; set; }
        
        public Character() { }

        public Character(Races input)
        {
            this.race = input;
            SetRaceBonus(race);
        }

        /// <summary>
        /// Applies racial bonuses
        /// </summary>
        /// <param name="input"></param>
        private void SetRaceBonus(Races input)
        {
            switch (input)
            {
                case Races.Dwarf:
                    constitution = 2;
                    speed = 25;
                    break;
                case Races.Elf:
                    dexterity = 2;
                    speed = 30;
                    break;
                case Races.Human:
                    strength = 1;
                    dexterity = 1;
                    constitution = 1;
                    intelligence = 1;
                    wisdom = 1;
                    charisma = 1;
                    speed = 30;
                    break;
            }
        }

        private void FindModifier()
        {

        }

    }
}

